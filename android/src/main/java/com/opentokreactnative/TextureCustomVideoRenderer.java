package com.opentokreactnative;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.facebook.react.bridge.Callback;
import com.opentok.android.TextureViewRenderer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public class TextureCustomVideoRenderer extends TextureViewRenderer {

    Context mContext;

    boolean mSaveScreenshot;

    Callback callback;

    public TextureCustomVideoRenderer(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    public void onFrame(Frame frame) {
        super.onFrame(frame);
        if (mSaveScreenshot) {

            ByteBuffer bb = frame.getBuffer();
            bb.clear();

            int width = frame.getWidth();
            int height = frame.getHeight();
            int half_width = (width + 1) >> 1;
            int half_height = (height +1) >> 1;
            int y_size = width * height;
            int uv_size = half_width * half_height;

            byte []yuv = new byte[y_size + uv_size * 2];
            bb.get(yuv);
            int[] intArray = new int[width*height];

            decodeYUV420(intArray, yuv, width, height);

            Bitmap bmp = Bitmap.createBitmap(intArray, width, height, Bitmap.Config.ARGB_8888);

            try {
                String path = mContext
                                .getApplicationInfo()
                                .dataDir + File.separator + "Documents/Livegenic/photos/";
                File f = new File(path);

                if (!f.exists()) {
                    f.mkdirs();
                }

                OutputStream fOutputStream = null;
                File file = new File(path, "livegenic_capture" + System.currentTimeMillis() + ".jpeg");
                fOutputStream = new FileOutputStream(file);

                bmp.compress(Bitmap.CompressFormat.PNG, 100, fOutputStream);

                fOutputStream.flush();
                fOutputStream.close();

                callback.invoke(file.getPath());
            } catch (Exception e) {
                e.printStackTrace();
                callback.invoke("error: " + e.getMessage());
                return;
            } finally {
                mSaveScreenshot = false;
            }
        }
    }

    static public void decodeYUV420(int[] rgba, byte[] yuv420, int width, int height) {
        int half_width = (width + 1) >> 1;
        int half_height = (height +1) >> 1;
        int y_size = width * height;
        int uv_size = half_width * half_height;

        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {

                double y = (yuv420[j * width + i]) & 0xff;
                double v = (yuv420[y_size + (j >> 1) * half_width + (i>>1)]) & 0xff;
                double u = (yuv420[y_size + uv_size + (j >> 1) * half_width + (i>>1)]) & 0xff;

                double r;
                double g;
                double b;

                r = y + 1.402 * (u-128);
                g = y - 0.34414*(v-128) - 0.71414*(u-128);
                b = y + 1.772*(v-128);

                if (r < 0) r = 0;
                else if (r > 255) r = 255;
                if (g < 0) g = 0;
                else if (g > 255) g = 255;
                if (b < 0) b = 0;
                else if (b > 255) b = 255;

                int ir = (int)r;
                int ig = (int)g;
                int ib = (int)b;
                rgba[j * width + i] = 0xff000000 | (ir << 16) | (ig << 8) | ib;
            }
        }
    }

    public void saveScreenshot(Boolean enableScreenshot, Callback callback) {
        mSaveScreenshot = enableScreenshot;
        this.callback = callback;
    }
}
